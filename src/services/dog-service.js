import axios from "axios";


const apiUrl = 'http://localhost:3000/dog';

export async function fetchDogs() {
    const response = await axios.get(apiUrl);
    return response.data;
}

export async function postDog(dog) {
    const response = await axios.post(apiUrl, dog);
    return response.data;
}

export async function deleteDog(dog) {
    const response = await axios.delete(apiUrl + '/'+dog.id);
    return response.data;
}